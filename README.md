## asciibry

A python text-based LBRY client for the terminal.

## license

https://www.gnu.org/licenses/gpl-3.0.txt

## dependencies

* python 3.7
* urwid
* requests